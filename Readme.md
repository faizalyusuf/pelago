1. Deploy certificate manager
```kubectl create namespace cert-manager```
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml
2. Create Issuer
kubectl apply -f cert-manager/issuer.yaml
3. Create registry namespace
kubectl create namespace registry
4. Create self-signed cert and key in registry namespace
kubectl apply -f secrets/certs.yaml
5. Create htpasswd
kubectl apply -f secrets/htpasswd.yaml
6. Create persistent volume and persistent volume claim
kubectl apply -f volumes/pv.yaml
kubectl apply -f volumes/pvc.yaml
7. Deploy Redis
kubectl apply -f redis/redis.yaml
8. Deploy registry
kubectl apply -f deployment.yaml
9. Deploy service
kubectl apply -f service.yaml
10. Deploy ingress
kubectl apply -f ingress.yaml
11. Deploy cleanup job
-----